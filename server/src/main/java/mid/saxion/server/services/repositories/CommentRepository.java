package mid.saxion.server.services.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mid.saxion.server.entities.Comment;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {

    @Override
    List<Comment> findAll();
    
}
