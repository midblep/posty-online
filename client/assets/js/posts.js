displayBlogposts();

async function displayBlogposts() {
    let element = document.getElementById("list");
    element.innerHTML = "";

    fetch('http://localhost:8080/posts')
    .then(response => response.json())
    .then(data => data.forEach(obj => {
        let rootNode = document.createElement("a");
        let titleNode = document.createElement("h2");
        let contentNode = document.createElement("p");
        let hrNode = document.createElement("hr");
        let authorNode = document.createElement("p");
        rootNode.href = "show.html?id=" + obj['id'];

        element.appendChild(rootNode);
        rootNode.appendChild(titleNode).textContent = obj['title'];
        rootNode.appendChild(contentNode).textContent = obj['content'].slice(0, 50);
        rootNode.appendChild(hrNode);

        fetch('http://localhost:8080/users/' + obj['user']['id']).then(response => response.json()).then(data => {
            rootNode.appendChild(authorNode).textContent = "Written by " + data['name'];
        });
    }));
}

async function query() {
    let query = document.getElementById("query").value;
    let element = document.getElementById("list");
    element.innerHTML = "No results found for query " + query + ".";

    if(query != "") {
        fetch('http://localhost:8080/posts?title=' + query)
        .then(response => response.json())
        .then(data => data.forEach(obj => {
            let rootNode = document.createElement("a");
            let titleNode = document.createElement("h2");
            let contentNode = document.createElement("p");
            let hrNode = document.createElement("hr");
            let authorNode = document.createElement("p");
            rootNode.href = "show.html?id=" + obj['id'];

            element.appendChild(rootNode);
            rootNode.appendChild(titleNode).textContent = obj['title'];
            rootNode.appendChild(contentNode).textContent = obj['content'].slice(0, 50);
            rootNode.appendChild(hrNode);

            fetch('http://localhost:8080/users/' + obj['user']['id']).then(response => response.json()).then(data => {
                rootNode.appendChild(authorNode).textContent = "Written by " + data['name'];
            });
        }));
    } else {
        displayBlogposts();
    }
}