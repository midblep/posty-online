package mid.saxion.server.services.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mid.saxion.server.entities.Post;

@Repository
public interface PostRepository extends CrudRepository<Post, Long> {
    
    @Override
    List<Post> findAll();
    
}