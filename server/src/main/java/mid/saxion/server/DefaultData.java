package mid.saxion.server;

import mid.saxion.server.entities.Post;
import mid.saxion.server.entities.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import mid.saxion.server.entities.Comment;
import mid.saxion.server.services.CommentService;
import mid.saxion.server.services.PostService;
import mid.saxion.server.services.UserService;

@Component
public class DefaultData implements ApplicationRunner {

    @Autowired
    private CommentService commentService;
    @Autowired
    private PostService postService;
    @Autowired
    private UserService userService;
    
    public void run(ApplicationArguments args) {
        // users
        User newUser1 = new User();
        newUser1.setName("Bart");
        userService.save(newUser1);

        User newUser2 = new User();
        newUser2.setName("Pascal van Ginkel");
        userService.save(newUser2);

        User newUser3 = new User();
        newUser3.setName("Mid");
        userService.save(newUser3);

        User newUser4 = new User();
        newUser4.setName("TheLegend27");
        userService.save(newUser4);

        // posts
        Post newPost1 = new Post();
        newPost1.setTitle("Very big welcome to Posty Online!");
        newPost1.setContent("We are honored to have you here with us!");
        newPost1.setUser(newUser2);
        postService.save(newPost1);

        Post newPost2 = new Post();
        newPost2.setTitle("I love Posty Online");
        newPost2.setContent("I heard they are 100% privacy");
        newPost2.setUser(newUser2);
        postService.save(newPost2);

        Post newPost3 = new Post();
        newPost3.setTitle("Example title example title");
        newPost3.setContent("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        newPost3.setUser(newUser3);
        postService.save(newPost3);

        // comments
        Comment newComment1 = new Comment();
        newComment1.setContent("Yo I love this place!");
        newComment1.setPost(newPost1);
        newComment1.setUser(newUser1);
        commentService.save(newComment1);

        Comment newComment2 = new Comment();
        newComment2.setContent("Very good article bro");
        newComment2.setPost(newPost1);
        newComment2.setUser(newUser4);
        commentService.save(newComment2);

        Comment newComment3 = new Comment();
        newComment3.setContent("I love this");
        newComment3.setPost(newPost2);
        newComment3.setUser(newUser3);
        commentService.save(newComment3);

        Comment newComment4 = new Comment();
        newComment4.setContent("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        newComment4.setPost(newPost3);
        newComment4.setUser(newUser1);
        commentService.save(newComment4);

        Comment newComment5 = new Comment();
        newComment5.setContent("Get out of my face bro, this is clearly a scam");
        newComment5.setPost(newPost1);
        newComment5.setUser(newUser1);
        commentService.save(newComment5);

        Comment newComment6 = new Comment();
        newComment6.setContent("LOVE YOUR WORK!");
        newComment6.setPost(newPost1);
        newComment6.setUser(newUser2);
        commentService.save(newComment6);
    }
}
