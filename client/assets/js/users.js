displayUsers();

async function displayUsers() {
    let element = document.getElementById("list");
    element.innerHTML = "";

    fetch('http://localhost:8080/users')
    .then(response => response.json())
    .then(data => data.forEach(obj => {
        let rootNode = document.createElement("a");
        let nameNode = document.createElement("h2");
        rootNode.href = "show.html?id=" + obj['id'];

        element.appendChild(rootNode);
        rootNode.appendChild(nameNode).textContent = obj['name'];
    }));
}

async function query() {
    let query = document.getElementById("query").value;
    let element = document.getElementById("list");
    element.innerHTML = "No results found for query " + query + ".";

    if(query != "") {
        fetch('http://localhost:8080/users?name=' + query)
        .then(response => response.json())
        .then(data => data.forEach(obj => {
            let rootNode = document.createElement("a");
            let nameNode = document.createElement("h2");
            rootNode.href = "show.html?id=" + obj['id'];

            element.appendChild(rootNode);
            rootNode.appendChild(nameNode).textContent = obj['name'];
        }));
    } else {
        displayUsers();
    }
}