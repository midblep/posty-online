package mid.saxion.server.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import mid.saxion.server.entities.User;
import mid.saxion.server.services.UserService;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins ="*", allowedHeaders="*")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<User> getAllUsers(@RequestParam(required = false) String name) {
        if(name != null) return this.userService.getAllByName(name);
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User findById(@PathVariable Long id) {
        User user = userService.getUserById(id);
        if(user == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id of " + id + " does not exist.");

        return user;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User createNewUser(@RequestBody User user) {
        if (user.getId() != null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You cannot supply an id.");
        if (user.getName() == "") throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A name is required.");

        return userService.save(user);
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable Long id, @RequestBody User user) {
        if (user.getId() != null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You cannot pass an id to change.");
        User userToUpdate = userService.getUserById(id);
        if(userToUpdate == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id of " + id + " does not exist.");

        if(user.getName() != null) userToUpdate.setName(user.getName());

        return userService.save(userToUpdate);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable Long id) {
        if(!userService.userExists(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id of " + id + " does not exist.");

        userService.deleteUser(id);
    }

}
