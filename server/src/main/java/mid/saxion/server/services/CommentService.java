package mid.saxion.server.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mid.saxion.server.entities.Comment;
import mid.saxion.server.services.repositories.CommentRepository;

@Service
public class CommentService {
    
    @Autowired
    private CommentRepository commentRepository;

    public List<Comment> getAll() {
        return commentRepository.findAll();
    }

    public List<Comment> getAllByContent(String content) {
        List<Comment> returnList = new ArrayList<Comment>();

        for(Comment comment : this.commentRepository.findAll()) {
            if(comment.getContent().equals(content)) returnList.add(comment);
        }

        return returnList;
    }

    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }

    public boolean commentExists(Long id) {
        if(this.commentRepository.findById(id).orElse(null) == null) return false;
        return true;
    }

    public Comment getCommentById(Long id) {
        return this.commentRepository.findById(id).orElse(null);
    }

    public void deleteComment(Long id) {
        commentRepository.deleteById(id);
    }
}
