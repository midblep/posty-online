async function prefillData() {
    let nameElement = document.getElementById("name");

    fetch('http://localhost:8080/users/' + id)
    .then(response => response.json())
    .then(data => {
        nameElement.value = data['name'];
    });
}

async function editUser() {
    let response;
    let errorMsg = document.getElementById("error");
    let name = document.getElementById("name").value;
    const editedUser = {"name": name};

    if(checkIfEdit() == false) 
    {  
        response = fetch('http://localhost:8080/users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(editedUser)
        });
    }
    else 
    {
        response = fetch('http://localhost:8080/users/' + id, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(editedUser)
        });
    }

    response.catch((error) => {
        errorMsg.innerHTML = "Something went wrong. Check if the server is turned on!";
        return;
    });

    response = await response;

    if(!response.ok) {
        errorMsg.innerHTML = "Error " + response.status + " something went wrong. Check if you filled in all fields correctly!";
        return;
    } else {
        const data = await response.json();
        window.location.replace("show.html?id=" + data['id']);
    }
}