displayBlogpost();
displayComments();

async function displayBlogpost() {
    let element = document.getElementById("blogpost");
    let editbtn = document.getElementById("editbtn");
    element.innerHTML = "";

    fetch('http://localhost:8080/posts/' + id)
    .then(response => response.json())
    .then(data => {
        let rootNode = document.createElement("div");
        let titleNode = document.createElement("h1");
        let contentNode = document.createElement("p");
        let authorNode = document.createElement("a");
        authorNode.href = "../users/show.html?id=" + data['user']['id'];

        element.appendChild(rootNode);
        rootNode.appendChild(titleNode).innerHTML = "ID: " + data['id'] + "<br> Title: " + data['title'];
        rootNode.appendChild(contentNode).textContent = data['content'];
        rootNode.appendChild(authorNode).textContent = "Written by " + data['user']['name'];

        editbtn.href = "edit.html?edit=true&id=" + id;
    });
}

async function displayComments() {
    let element = document.getElementById("comments");
    element.innerHTML = "";

    fetch('http://localhost:8080/comments')
    .then(response => response.json())
    .then(data => data.forEach(obj => {
        if(obj['post']['id'] == id) {
            let rootNode = document.createElement("div");
            let contentNode = document.createElement("p");
            let authorNode = document.createElement("a");
            let deleteNode = document.createElement("a");
            deleteNode.onclick = function(event) {
                deleteComment(obj['id']);
            };
            deleteNode.href = "#";
            authorNode.href = "../users/show.html?id=" + obj['user']['id'];
    
            element.appendChild(rootNode);
            rootNode.appendChild(contentNode).textContent = obj['content'];
            rootNode.appendChild(authorNode).textContent = "Written by " + obj['user']['name'] + " |";
            rootNode.appendChild(deleteNode).textContent = "| Delete Comment";
        }
    }));
}

async function deleteComment(commentId) {
    await fetch('http://localhost:8080/comments/' + commentId, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(function() {
        displayComments();
    })
    .catch((error) => {
        console.error('Error:');
    });
}

async function comment() {
    let response;
    let errorMsg = document.getElementById("error");
    let content = document.getElementById("content").value;
    let userid = document.getElementById("userid").value;
    const newComment = {
        "content": content,
        "post": {
            "id": id
        },
        "user": {
            "id": userid
        }
    };

    response = fetch('http://localhost:8080/comments', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(newComment)
    });

    response.catch((error) => {
        errorMsg.innerHTML = "Something went wrong. Check if the server is turned on!";
        return;
    });

    response = await response;

    if(!response.ok) {
        errorMsg.innerHTML = "Error " + response.status + " something went wrong. Check if you filled in all fields correctly!";
        return;
    }
}