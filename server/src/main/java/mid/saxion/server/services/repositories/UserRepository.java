package mid.saxion.server.services.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mid.saxion.server.entities.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    
    @Override
    List<User> findAll();
}