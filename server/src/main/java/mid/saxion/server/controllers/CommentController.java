package mid.saxion.server.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import mid.saxion.server.entities.Comment;
import mid.saxion.server.services.CommentService;

@RestController
@RequestMapping("/comments")
@CrossOrigin(origins ="*", allowedHeaders="*")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping
    public List<Comment> getAllComments(@RequestParam(required = false) String content) {
        if(content != null) return commentService.getAllByContent(content);
        return commentService.getAll();
    }

    @GetMapping("/{id}")
    public Comment findByTitle(@PathVariable Long id) {
        Comment comment = commentService.getCommentById(id);
        if(comment == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Comment with id of " + id + " does not exist.");

        return comment;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Comment createNewComment(@RequestBody Comment comment) {
        if (comment.getId() != null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You cannot supply an id.");
        if (comment.getContent() == "") throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A content field is required.");

        return commentService.save(comment);
    }

    @PutMapping("/{id}")
    public Comment updateComment(@PathVariable Long id, @RequestBody Comment comment) {
        if (comment.getId() != null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You cannot pass an id to change.");
        Comment commentToUpdate = commentService.getCommentById(id);
        if(commentToUpdate == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Comment with id of " + id + " does not exist.");

        if(comment.getContent() != null) commentToUpdate.setContent(comment.getContent());
        if(comment.getUser() != null) commentToUpdate.setUser(comment.getUser());
        if(comment.getPost() != null) commentToUpdate.setPost(comment.getPost());

        return commentService.save(commentToUpdate);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteComment(@PathVariable Long id) {
        if(!commentService.commentExists(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Comment with id of " + id + " does not exist.");

        commentService.deleteComment(id);
    }
    
}
