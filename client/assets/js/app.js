count();

const urlSearchParams = new URLSearchParams(window.location.search);
const id = urlSearchParams.get('id');
if(checkIfEdit() == true) prefillData();

function checkIfEdit() {
    if(urlSearchParams.get('edit') == "true" && id != undefined) return true;
    return false;
}

async function count() {
    var countPosts = document.getElementById("countPosts");
    var countUsers = document.getElementById("countUsers");
    var countComments = document.getElementById("countComments");

    fetch('http://localhost:8080/posts')
    .then(response => response.json())
    .then(data => countPosts.textContent = Object.keys(data).length);

    fetch('http://localhost:8080/users')
    .then(response => response.json())
    .then(data => countUsers.textContent = Object.keys(data).length);
}