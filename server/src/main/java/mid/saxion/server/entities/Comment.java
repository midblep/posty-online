package mid.saxion.server.entities;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity(name = "comments")
public class Comment {
    // unique identifier of a comment
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // contents of a comment
    @Column(nullable = false)
    private String content;

    // user who made the comment
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    // post the comment was made on
    @ManyToOne
    @JoinColumn(name="post_id")
    private Post post;

    // getters and setters
    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    public Long getId() {
        return this.id;
    }

    public Post getPost() {
        return this.post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
