School project website.
Backend uses Spring Boot. Run it by executing the ServerApplication.java class.
Frontend uses vanilla HTML/CSS + JS and can run without webserver in the browser.