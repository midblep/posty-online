package mid.saxion.server.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mid.saxion.server.entities.User;
import mid.saxion.server.services.repositories.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getAll() {
        return this.userRepository.findAll();
    }

    public List<User> getAllByName(String name) {
        List<User> returnList = new ArrayList<User>();

        for(User user : this.userRepository.findAll()) {
            if(user.getName().equals(name)) returnList.add(user);
        }

        return returnList;
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public boolean userExists(Long id) {
        if(this.userRepository.findById(id).orElse(null) == null) return false;
        return true;
    }

    public User getUserById(Long id) {
        return this.userRepository.findById(id).orElse(null);
    }

    public void deleteUser(Long id) {
        this.userRepository.deleteById(id);
    }
}
