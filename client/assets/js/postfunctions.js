async function prefillData() {
    let titleElement = document.getElementById("title");
    let contentElement = document.getElementById("content");
    let useridElement = document.getElementById("userid");

    fetch('http://localhost:8080/posts/' + id)
    .then(response => response.json())
    .then(data => {
        titleElement.value = data['title'];
        contentElement.value = data['content'];
        useridElement.value = data['user']['id'];
    });
}

async function editPost() {
    let response;
    let errorMsg = document.getElementById("error");
    let title = document.getElementById("title").value;
    let content = document.getElementById("content").value;
    let userid = document.getElementById("userid").value;
    const editedPost = {
        "title": title,
        "content": content,
        "user": {
            "id": userid
        }
    };

    if(checkIfEdit() == false) 
    {  
        response = fetch('http://localhost:8080/posts', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(editedPost)
        });
    }

    else 
    {
        response = fetch('http://localhost:8080/posts/' + id, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(editedPost)
        });
    }

    response.catch((error) => {
        errorMsg.innerHTML = "Something went wrong. Check if the server is turned on!";
    });

    response = await response;

    if(!response.ok) {
        errorMsg.innerHTML = "Error " + response.status + " something went wrong. Check if you filled in all fields correctly!";
    } else {
        const data = await response.json();
        window.location.replace("show.html?id=" + data['id']);
    }
}