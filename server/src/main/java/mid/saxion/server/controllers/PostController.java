package mid.saxion.server.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import mid.saxion.server.entities.Post;
import mid.saxion.server.services.PostService;

@RestController
@RequestMapping("/posts")
@CrossOrigin(origins ="*", allowedHeaders="*")
public class PostController {
    
    @Autowired
    private PostService postService;

    @GetMapping
    public List<Post> getAllPosts(@RequestParam(required = false) String title) {
        if(title != null) return postService.getAllByTitle(title);
        return postService.getAll();
    }

    @GetMapping("/{id}")
    public Post findById(@PathVariable Long id) {
        Post post = postService.getPostById(id);
        if(post == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Post with id of " + id + " does not exist.");

        return post;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Post createNewPost(@RequestBody Post post) {
        if (post.getId() != null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You cannot supply an id.");
        if (post.getTitle() == "") throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A title field is required.");
        if (post.getContent() == "") throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A content field is required.");

        return postService.save(post);
    }

    @PutMapping("/{id}")
    public Post updatePost(@PathVariable Long id, @RequestBody Post post) {
        if (post.getId() != null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You cannot pass an id to change.");
        Post postToUpdate = postService.getPostById(id);
        if(postToUpdate == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Post with id of " + id + " does not exist.");

        if(post.getTitle() != null) postToUpdate.setTitle(post.getTitle());
        if(post.getContent() != null) postToUpdate.setContent(post.getContent());
        if(post.getUser() != null) postToUpdate.setUser(post.getUser());

        return postService.save(post);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePost(@PathVariable Long id) {
        if(!postService.postExists(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Post with id of " + id + " does not exist.");

        postService.deletePost(id);
    }

}
