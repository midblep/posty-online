displayUser();
displayUserPosts();
displayUserComments();

async function displayUser() {
    let element = document.getElementById("user");
    let editbtn = document.getElementById("editbtn");
    element.innerHTML = "";

    fetch('http://localhost:8080/users/' + id)
    .then(response => response.json())
    .then(data => {
        let rootNode = document.createElement("div");
        let nameNode = document.createElement("h2");

        element.appendChild(rootNode);
        rootNode.appendChild(nameNode).innerHTML = "ID: " + data['id'] + "<br> Name: " + data['name'];

        editbtn.href = "edit.html?edit=true&id=" + id;
    });
}

async function displayUserPosts() {
    let element = document.getElementById("posts");
    element.innerHTML = "";

    fetch('http://localhost:8080/posts')
    .then(response => response.json())
    .then(data => data.forEach(obj => {
        if(obj['user']['id'] == id) {
            let rootNode = document.createElement("a");
            let titleNode = document.createElement("h2");
            let contentNode = document.createElement("p");
            rootNode.href = "../posts/show.html?id=" + obj['id'];
    
            element.appendChild(rootNode);
            rootNode.appendChild(titleNode).textContent = obj['title'];
            rootNode.appendChild(contentNode).textContent = obj['content'].slice(0, 50);
        }
    }))
    .then(function() {
        if(element.innerHTML === "") {
            let noPosts = document.createElement("span");
            element.appendChild(noPosts).textContent = "This user has no posts.";
        }
    })
    .catch((error) => {
        element.appendChild(noPosts).textContent = "Internal Server Error";
    });
}

async function displayUserComments() {
    let element = document.getElementById("comments");
    element.innerHTML = "";

    fetch('http://localhost:8080/comments')
    .then(response => response.json())
    .then(data => data.forEach(obj => {
        if(obj['user']['id'] == id) {
            console.log(obj);
            let rootNode = document.createElement("div");
            let contentNode = document.createElement("p");
            let linkNode = document.createElement("a");
            linkNode.href = "../posts/show.html?id=" + obj['post']['id'];

    
            element.appendChild(rootNode);
            rootNode.appendChild(contentNode).textContent = obj['content'];

            fetch('http://localhost:8080/posts/' + obj['post']['id'])
            .then(response => response.json())
            .then(data => {
                rootNode.appendChild(linkNode).textContent = "Posted on " + data['title'];
            });
        }
    }))    
    .then(function() {
        if(element.innerHTML === "") {
            let noPosts = document.createElement("span");
            element.appendChild(noPosts).textContent = "This user has no comments.";
        }
    })
    .catch((error) => {
        element.appendChild(noPosts).textContent = "Internal Server Error";
    });
}

async function deleteUser() {
    const response = await fetch('http://localhost:8080/users/' + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })

    window.location.replace("../index.html");
}