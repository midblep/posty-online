package mid.saxion.server.entities;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

import javax.persistence.*;

@Entity(name = "posts")
public class Post {
    // unique identifier for the post
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // title of the post
    @Column(nullable = false)
    private String title;

    // contents of the post
    @Column(columnDefinition = "TEXT", nullable = false)
    private String content;

    // user who made the post
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    // all comments of a post
    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    private List<Comment> comments;

    // getters and setters
    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public Long getId() {
        return this.id;
    }

    public List<Comment> getComments() {
        return this.comments;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
