package mid.saxion.server.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mid.saxion.server.entities.Post;
import mid.saxion.server.services.repositories.PostRepository;

@Service
public class PostService {

    @Autowired
    private PostRepository postRepository;

    public List<Post> getAll() {
        return this.postRepository.findAll();
    }

    public List<Post> getAllByTitle(String title) {
        List<Post> returnList = new ArrayList<Post>();

        for(Post post : this.postRepository.findAll()) {
            if(post.getTitle().equals(title)) returnList.add(post);
        }

        return returnList;
    }

    public Post save(Post post) {
        return postRepository.save(post);
    }

    public boolean postExists(Long id) {
        if(this.postRepository.findById(id).orElse(null) == null) return false;
        return true;
    }

    public Post getPostById(Long id) {
        return this.postRepository.findById(id).orElse(null);
    }

    public void deletePost(Long id) {
        this.postRepository.deleteById(id);
    }
}
